set VCPKG_DIR=E:\vcpkg
set NINJA_DIR=E:\mytools
set PLATFORM=x64
set GENERATOR=Ninja

if not exist %NINJA_DIR%\ mkdir %NINJA_DIR%
cd %NINJA_DIR%
if not exist ninja.exe powershell -Command wget https://github.com/ninja-build/ninja/releases/download/v1.8.2/ninja-win.zip
if not exist ninja.exe 7z x ninja-win.zip
set PATH=%NINJA_DIR%;%PATH%

cd %VCPKG_DIR%
git pull
echo.set(VCPKG_BUILD_TYPE release)>> %VCPKG_DIR%\triplets\%PLATFORM%-windows.cmake
.\bootstrap-vcpkg.bat
vcpkg version
cd %~dp0

vcpkg install ^
boost-system ^
boost-filesystem ^
boost-thread ^
boost-date-time ^
boost-iostreams ^
boost-chrono ^
boost-asio ^
boost-dynamic-bitset ^
boost-foreach ^
boost-graph ^
boost-interprocess ^
boost-multi-array ^
boost-ptr-container ^
boost-random ^
boost-signals2 ^
eigen3 ^
flann ^
qhull ^
gtest ^
--triplet %PLATFORM%-windows

vcpkg list
set PATH=%VCPKG_DIR%\installed\%PLATFORM%-windows\bin;%PATH%

pip install -r requirements.txt
rem pip install -e .
python setup.py build_ext -i
python setup.py install
rem python setup.py bdist_wheel
nosetests -A "not pcl_ver_0_4"
