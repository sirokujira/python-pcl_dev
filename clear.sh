export PACKAGENAME=python-pcl
export SOURCENAME=pcl
sudo rm -rf ${SOURCENAME}/__pycache__
sudo rm ${SOURCENAME}/*.so
sudo rm cythonize.json
sudo rm -rf build
sudo rm -rf dist
# sudo rm -rf ${PACKAGENAME}.egg-info
sudo rm -rf python_pcl.egg-info
sudo rm -rf .eggs
sudo pip uninstall ${PACKAGENAME} -y
