# -*- coding: utf-8 -*-
cimport _pcl
cimport pcl_defs as cpp
import numpy as np
cimport numpy as cnp

cnp.import_array()

cimport pcl_visualization as _pcl_visualization
cimport pcl_visualization_defs as pcl_vis

from boost_shared_ptr cimport sp_assign


class PointCloudColorHandleringCustom:
    """Handler for predefined user colors. 
    """
    # # cdef pcl_vis.PointCloudColorHandlerCustom_t *me
    # cdef pcl_vis.PointCloudColorHandlerCustom_Ptr_t thisptr_shared     # PointCloudColorHandlerCustom[PointXYZ]

    # # cdef inline PointCloudColorHandlerCustom[cpp.PointXYZ] *thisptr(self):
    # # pcl_visualization_defs
    # cdef inline pcl_vis.PointCloudColorHandlerCustom[cpp.PointXYZ] *thisptr(self):
    #     # Shortcut to get raw pointer to underlying PointCloudColorHandlerCustom<PointXYZ>.
    #     return self.thisptr_shared.get()
    def __cinit__(self, _pcl.PointCloud pc, int r, int g, int b):
        sp_assign(self.thisptr_shared, new pcl_vis.PointCloudColorHandlerCustom[cpp.PointXYZ](pc.thisptr_shared, r, g, b))
        # sp_assign(self.thisptr_shared, new pcl_vis.PointCloudColorHandlerCustom_Ptr_t(pc.thisptr_shared, r, g, b))
        pass

    # def __cinit__(self, _pcl.RangeImages rangeImage, int r, int g, int b):
    #     sp_assign(self.thisptr_shared, new pcl_vis.PointCloudColorHandlerCustom[cpp.PointWithViewpoint](rangeImage.thisptr_shared, r, g, b))
    #     pass

    def __dealloc__(self):
        # del self.me
        pass


