# -*- coding: utf-8 -*-

### include ###
# common?
# include "Vertices.pxi"
include "PointXYZtoPointXYZ.pxi"
# Segmentation
include "Segmentation_180.pxi"
include "SegmentationNormal_180.pxi"
include "EuclideanClusterExtraction_180.pxi"
include "RegionGrowing_180.pxi"
# Filters
include "StatisticalOutlierRemovalFilter_180.pxi"
include "VoxelGridFilter_180.pxi"
include "PassThroughFilter_180.pxi"
include "ApproximateVoxelGrid_180.pxi"
# Kdtree
# same from 1.6 to 1.8
# include "KdTree.pxi"
include "KdTree_FLANN.pxi"
# Octree
include "OctreePointCloud_180.pxi"
include "OctreePointCloud2Buf_180.pxi"
include "OctreePointCloudSearch_180.pxi"
include "OctreePointCloudChangeDetector_180.pxi"
# Filters
include "CropHull_180.pxi"
include "CropBox_180.pxi"
include "ProjectInliers_180.pxi"
include "RadiusOutlierRemoval_180.pxi"
include "ConditionAnd_180.pxi"
# include "ConditionalRemoval_180.pxi"
# Surface
include "ConcaveHull_180.pxi"
include "MovingLeastSquares_180.pxi"
# RangeImage
# include "RangeImages_180.pxi"


# Registration
include "GeneralizedIterativeClosestPoint_180.pxi"
include "IterativeClosestPoint_180.pxi"
include "IterativeClosestPointNonLinear_180.pxi"
# SampleConsensus
# same from 1.6 to 1.8
include "RandomSampleConsensus.pxi"
include "SampleConsensusModelPlane.pxi"
include "SampleConsensusModelSphere.pxi"
include "SampleConsensusModelCylinder.pxi"
include "SampleConsensusModelLine.pxi"
include "SampleConsensusModelRegistration.pxi"
include "SampleConsensusModelStick.pxi"
# Features
include "NormalEstimation_180.pxi"
include "VFHEstimation_180.pxi"
include "IntegralImageNormalEstimation_180.pxi"
include "MomentOfInertiaEstimation_180.pxi"

# keyPoint
include "HarrisKeypoint3D_180.pxi"
# execute NG?
# include "UniformSampling_180.pxi"

# Registration
include "NormalDistributionsTransform_180.pxi"
# pcl 1.7.2?
# include "NormalDistributionsTransform.pxi"
# visual
# include "PointCloudColorHandlerCustoms.pxi"
###


