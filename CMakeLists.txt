# Defines the CMake commands/policies
cmake_minimum_required(VERSION 2.8.8)

# Set the project name
project(CYTHON_CMAKE_EXAMPLE)

enable_language(C CXX)
set(CMAKE_CXX_STANDARD 11) # C++14...
set(CMAKE_CXX_STANDARD_REQUIRED ON) #...is required...
set(CMAKE_CXX_EXTENSIONS OFF) #...without compiler extensions like gnu++11
# use c++11 and not blank space define.
# https://en.cppreference.com/w/cpp/language/user_literal
# set(CMAKE_CXX_FLAGS " -Wno-reserved-user-defined-literal -std=c++11")
# https://github.com/IntelVCL/Open3D/issues/712
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-deprecated-declarations")

set(EXTERNAL_MODULE_PATH)
set(EXTERNAL_PKG_INCLUDE "")
set(EXTERNAL_PKG_LIBRARYDIR "")
set(EXTERNAL_PKG_LIBRARIES "")
set(EXTERNAL_PKG_DEFINITIONS "")

# Windows only?
list(APPEND EXTERNAL_MODULE_PATH "env:${PCL_ROOT}/cmake")

# Make the scripts available in the 'cmake' directory available for the
# 'include()' command, 'find_package()' command.
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_LIST_DIR}/cmake" ${EXTERNAL_MODULE_PATH})

# Include the CMake script UseCython.cmake.  This defines add_cython_module().
# Instruction for use can be found at the top of cmake/UseCython.cmake.
include(UseCython)

find_package(Numpy REQUIRED)
# message(STATUS ${PYTHON_NUMPY_FOUND})
# message(STATUS ${PYTHON_NUMPY_INCLUDE_DIR})
list(APPEND EXTERNAL_PKG_INCLUDE ${PYTHON_NUMPY_INCLUDE_DIR})

find_package(PCL REQUIRED)
# message(STATUS ${PCL_INCLUDE_DIRS})
# message(STATUS ${PCL_LIBRARY_DIRS})
# message(STATUS ${PCL_DEFINITIONS})
# message(STATUS ${PCL_LIBRARIES})
list(APPEND EXTERNAL_PKG_INCLUDE ${PCL_INCLUDE_DIRS})
list(APPEND EXTERNAL_PKG_LIBRARYDIR ${PCL_LIBRARY_DIRS})
list(APPEND EXTERNAL_PKG_LIBRARIES ${PCL_LIBRARIES})
list(APPEND EXTERNAL_PKG_DEFINITIONS ${PCL_DEFINITIONS})

find_package(OpenMP QUIET)
if (OPENMP_FOUND)
    message(STATUS "Using installed OpenMP ${OpenMP_VERSION}")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")

    set(Config_Open3D_C_FLAGS          ${OpenMP_C_FLAGS})
    set(Config_Open3D_CXX_FLAGS        ${OpenMP_CXX_FLAGS})
    set(Config_Open3D_EXE_LINKER_FLAGS ${OpenMP_EXE_LINKER_FLAGS})
else ()
    message(STATUS "OpenMP NOT found")
endif ()

# load the module to use pkg-config
# find_package(PkgConfig)

# search library with pkg-config and name its results "LIBSAMPLE"
# pkg_check_modules(LIBSAMPLE REQUIRED libsample)

include_directories(${CYTHON_CMAKE_EXAMPLE_SOURCE_DIR}/include ${EXTERNAL_PKG_INCLUDE})
link_directories(${EXTERNAL_PKG_LIBRARYDIR})
add_definitions(${EXTERNAL_PKG_DEFINITIONS})

# With CMake, a clean separation can be made between the source tree and the
# build tree.  When all source is compiled, as with pure C/C++, the source is
# no-longer needed in the build tree.  However, with pure *.py source, the
# source is processed directly.  To handle this, we reproduce the availability
# of the source files in the build tree.
add_custom_target( ReplicatePythonSourceTree ALL ${CMAKE_COMMAND} -P
  ${CMAKE_CURRENT_SOURCE_DIR}/cmake/ReplicatePythonSourceTree.cmake
  ${CMAKE_CURRENT_BINARY_DIR}
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} )


enable_testing()
find_file( NOSETESTS_EXECUTABLE nosetests )
add_test( nosetests "${NOSETESTS_EXECUTABLE}" -v --with-xunit )

# Process the CMakeLists.txt in the 'src' and 'bin' directory.
add_subdirectory( src )
# add_subdirectory( bin )
# add_subdirectory( pcl )
