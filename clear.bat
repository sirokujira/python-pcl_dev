set PACKAGENAME=python-pcl
set SOURCENAME=pcl
rd /s /q %SOURCENAME%\__pycache__
del %SOURCENAME%\*.pyd
del cythonize.json
rd /s /q build
rem rd /s /q _skbuild
rd /s /q dist
rd /s /q dlls
rem rd /s /q %PACKAGENAME%.egg-info
rd /s /q python_pcl.egg-info
rd /s /q .eggs
pip uninstall %PACKAGENAME% -y