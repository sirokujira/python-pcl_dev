# -*- coding: utf-8 -*-
from __future__ import print_function
from collections import defaultdict
from Cython.Distutils import build_ext
from setuptools import setup
from setuptools import Extension
from setuptools import find_packages
# using scikit-build
# from skbuild import setup

import subprocess
import numpy
import sys
import os
import time
import contextlib

import re
import sysconfig
import platform

from distutils.version import LooseVersion

from setuptools.command.build_ext import build_ext
from setuptools.command.test import test as TestCommand
from shutil import copyfile, copymode

import shutil
from ctypes.util import find_library
setup_requires = []
install_requires = [
    'filelock',
    'nose',
    'numpy',
    'Cython',
]


MOD_NAMES = [ 
    '_pcl', 
    'pcl_visualization', 
]


class CMakeExtension(Extension):
    def __init__(self, name, sourcedir=''):
        Extension.__init__(self, name, sources=[])
        self.sourcedir = os.path.abspath(sourcedir)


class CMakeBuild(build_ext):
    def run(self):
        # pip use?
        # https://github.com/scikit-build/cmake-python-distributions/blob/master/tests/test_cmake.py
        # import cmake

        try:
            # console
            out = subprocess.check_output(['cmake', '--version'])
        except OSError:
            raise RuntimeError(
                "CMake must be installed to build the following extensions: " +
                ", ".join(e.name for e in self.extensions))

        if platform.system() == "Windows":
            cmake_version = LooseVersion(re.search(r'version\s*([\d.]+)',
                                         out.decode()).group(1))
            if cmake_version < '3.1.0':
                raise RuntimeError("CMake >= 3.1.0 is required on Windows")

        for ext in self.extensions:
            self.build_extension(ext)


    def build_extension(self, ext):
        root = os.path.abspath(os.path.dirname(__file__))

        # extdir = os.path.abspath(
        #     os.path.dirname(self.get_ext_fullpath(ext.name)))
        # print(ext)
        # print(ext.name)
        extdir = os.path.abspath(
            os.path.dirname(self.get_ext_fullpath(ext.name)))

        cmake_args = ['-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=' + extdir,
                      '-DPYTHON_EXECUTABLE=' + sys.executable]

        cfg = 'Debug' if self.debug else 'Release'
        build_args = ['--config', cfg]

        if platform.system() == "Windows":
            # cmake_args += ['-DCMAKE_HAVE_PTHREAD_H:INTERNAL=E:/python-pcl2/include/pthread.h']
            # cmake_args += ['-DCMAKE_USE_WIN32_THREADS_INIT:BOOL=ON']
            # cmake_args += ['-DCMAKE_USE_PTHREADS_INIT:BOOL=OFF']
            # use vcpkg(check env VCPKG_DIR?)
            # cmake_args += ['-DCMAKE_TOOLCHAIN_FILE=${VCPKG_DIR}/scripts/buildsystems/vcpkg.cmake']
            cmake_args += ['-DCMAKE_LIBRARY_OUTPUT_DIRECTORY_{}={}'.format(
                cfg.upper(),
                extdir)]
            if sys.maxsize > 2**32:
                cmake_args += ['-A', 'x64']
            build_args += ['--', '/m']
        else:
            cmake_args += ['-DCMAKE_BUILD_TYPE=' + cfg]
            build_args += ['--', '-j2']

        env = os.environ.copy()

        env['CXXFLAGS'] = '{} -DVERSION_INFO=\\"{}\\"'.format(
            env.get('CXXFLAGS', ''),
            self.distribution.get_version())

        if not os.path.exists(self.build_temp):
            os.makedirs(self.build_temp)

        if not os.path.exists(os.path.join(root, 'PKG-INFO')):
            print('Cythonizing sources')
            p = subprocess.call([sys.executable,
                             os.path.join(root, 'bin', 'cythonize.py'),
                             'template'], env=env)
            if p != 0:
                raise RuntimeError('Running cythonize failed')

        p = subprocess.check_call(['cmake', ext.sourcedir] + cmake_args,
                                  cwd=self.build_temp, env=env)
        if p != 0:
            raise RuntimeError('Running cmake create failed')

        p = subprocess.check_call(['cmake', '--build', '.'] + build_args,
                                  cwd=self.build_temp)
        if p != 0:
            raise RuntimeError('Running cmake generate failed')

        print()  # Add an empty line for cleaner output


def clean(path): 
    for name in MOD_NAMES: 
        name = name.replace('.', '/') 
        for ext in ['.so', '.html', '.cpp', '.c']: 
            file_path = os.path.join(path, name + ext) 
            if os.path.exists(file_path): 
                os.unlink(file_path) 


@contextlib.contextmanager
def chdir(new_dir):
    old_dir = os.getcwd()
    try:
        os.chdir(new_dir)
        sys.path.insert(0, new_dir)
        yield
    finally:
        del sys.path[0]
        os.chdir(old_dir)


if __name__ == '__main__': 
    # setup_package()
    boost_version = '1_64'
    vtk_version = '8.0'
    data_files = None
    libreleases = ['pcl_common_release', 'pcl_features_release', 'pcl_filters_release', 'pcl_io_release', 'pcl_io_ply_release', 'pcl_kdtree_release', 'pcl_keypoints_release', 'pcl_ml_release', 'pcl_octree_release', 'pcl_outofcore_release', 'pcl_people_release', 'pcl_recognition_release', 'pcl_registration_release', 'pcl_sample_consensus_release', 'pcl_search_release', 'pcl_segmentation_release', 'pcl_stereo_release', 'pcl_surface_release', 'pcl_tracking_release', 'pcl_visualization_release', 'flann', 'flann_s', 'vtkalglib-' + vtk_version, 'vtkChartsCore-' + vtk_version, 'vtkCommonColor-' + vtk_version, 'vtkCommonComputationalGeometry-' + vtk_version, 'vtkCommonCore-' + vtk_version, 'vtkCommonDataModel-' + vtk_version, 'vtkCommonExecutionModel-' + vtk_version, 'vtkCommonMath-' + vtk_version, 'vtkCommonMisc-' + vtk_version, 'vtkCommonSystem-' + vtk_version, 'vtkCommonTransforms-' + vtk_version, 'vtkDICOMParser-' + vtk_version, 'vtkDomainsChemistry-' + vtk_version, 'vtkexoIIc-' + vtk_version, 'vtkexpat-' + vtk_version, 'vtkFiltersAMR-' + vtk_version, 'vtkFiltersCore-' + vtk_version, 'vtkFiltersExtraction-' + vtk_version, 'vtkFiltersFlowPaths-' + vtk_version, 'vtkFiltersGeneral-' + vtk_version, 'vtkFiltersGeneric-' + vtk_version, 'vtkFiltersGeometry-' + vtk_version, 'vtkFiltersHybrid-' + vtk_version, 'vtkFiltersHyperTree-' + vtk_version, 'vtkFiltersImaging-' + vtk_version, 'vtkFiltersModeling-' + vtk_version, 'vtkFiltersParallel-' + vtk_version, 'vtkFiltersParallelImaging-' + vtk_version, 'vtkFiltersProgrammable-' + vtk_version, 'vtkFiltersSelection-' + vtk_version, 'vtkFiltersSMP-' + vtk_version, 'vtkFiltersSources-' + vtk_version, 'vtkFiltersStatistics-' + vtk_version, 'vtkFiltersTexture-' + vtk_version, 'vtkFiltersVerdict-' + vtk_version, 'vtkfreetype-' + vtk_version, 'vtkGeovisCore-' + vtk_version, 'vtkgl2ps-' + vtk_version, 'vtkhdf5-' + vtk_version,
                   'vtkhdf5_hl-' + vtk_version, 'vtkImagingColor-' + vtk_version, 'vtkImagingCore-' + vtk_version, 'vtkImagingFourier-' + vtk_version, 'vtkImagingGeneral-' + vtk_version, 'vtkImagingHybrid-' + vtk_version, 'vtkImagingMath-' + vtk_version, 'vtkImagingMorphological-' + vtk_version, 'vtkImagingSources-' + vtk_version, 'vtkImagingStatistics-' + vtk_version, 'vtkImagingStencil-' + vtk_version, 'vtkInfovisCore-' + vtk_version, 'vtkInfovisLayout-' + vtk_version, 'vtkInteractionImage-' + vtk_version, 'vtkInteractionStyle-' + vtk_version, 'vtkInteractionWidgets-' + vtk_version, 'vtkIOAMR-' + vtk_version, 'vtkIOCore-' + vtk_version, 'vtkIOEnSight-' + vtk_version, 'vtkIOExodus-' + vtk_version, 'vtkIOExport-' + vtk_version, 'vtkIOGeometry-' + vtk_version, 'vtkIOImage-' + vtk_version, 'vtkIOImport-' + vtk_version, 'vtkIOInfovis-' + vtk_version, 'vtkIOLegacy-' + vtk_version, 'vtkIOLSDyna-' + vtk_version, 'vtkIOMINC-' + vtk_version, 'vtkIOMovie-' + vtk_version, 'vtkIONetCDF-' + vtk_version, 'vtkIOParallel-' + vtk_version, 'vtkIOParallelXML-' + vtk_version, 'vtkIOPLY-' + vtk_version, 'vtkIOSQL-' + vtk_version, 'vtkIOVideo-' + vtk_version, 'vtkIOXML-' + vtk_version, 'vtkIOXMLParser-' + vtk_version, 'vtkjpeg-' + vtk_version, 'vtkjsoncpp-' + vtk_version, 'vtklibxml2-' + vtk_version, 'vtkmetaio-' + vtk_version, 'vtkNetCDF-' + vtk_version, 'vtkoggtheora-' + vtk_version, 'vtkParallelCore-' + vtk_version, 'vtkpng-' + vtk_version, 'vtkproj4-' + vtk_version, 'vtkRenderingAnnotation-' + vtk_version, 'vtkRenderingContext2D-' + vtk_version, 'vtkRenderingContextOpenGL-' + vtk_version, 'vtkRenderingCore-' + vtk_version, 'vtkRenderingFreeType-' + vtk_version, 'vtkRenderingGL2PS-' + vtk_version, 'vtkRenderingImage-' + vtk_version, 'vtkRenderingLabel-' + vtk_version, 'vtkRenderingLIC-' + vtk_version, 'vtkRenderingLOD-' + vtk_version, 'vtkRenderingOpenGL-' + vtk_version, 'vtkRenderingVolume-' + vtk_version, 'vtkRenderingVolumeOpenGL-' + vtk_version, 'vtksqlite-' + vtk_version, 'vtksys-' + vtk_version, 'vtktiff-' + vtk_version, 'vtkverdict-' + vtk_version, 'vtkViewsContext2D-' + vtk_version, 'vtkViewsCore-' + vtk_version, 'vtkViewsInfovis-' + vtk_version, 'vtkzlib-' + vtk_version]
    if sys.platform == 'win32':
        # copy the pcl dll to local subfolder so that it can be added to the package through the data_files option
        listDlls = []
        if not os.path.isdir('./dlls'):
            os.mkdir('./dlls')
        for dll in libreleases:
            pathDll = find_library(dll)
            if not pathDll is None:
                shutil.copy2(pathDll, './dlls')
                listDlls.append(os.path.join('.\\dlls', dll+'.dll'))
        # the path is relative to the python root folder
        data_files = [('Lib/site-packages', listDlls)]
    else:
        listDlls = []


    setup(name='python-pcl', # not set Unknown modules
          description='A PointCloudLibrary use Python modules',
          url='http://github.com/strawlab/python-pcl',
          version='0.4',
          author='John Stowers',
          author_email='john.stowers@gmail.com',
          maintainer='Tooru Oonuma',
          maintainer_email='t753github@gmail.com',
          license='BSD',
          # use vcpkg?
          # cmake_args=['-DCMAKE_TOOLCHAIN_FILE:FILEPATH=E:/vcpkg/scripts/buildsystems/vcpkg.cmake'],
          # CMakeLists define only?
          packages=find_packages(),
          # packages=[
          #     "pcl",
          # ],
          zip_safe=False,
          setup_requires=setup_requires,
          install_requires=install_requires,
          classifiers=[
              'Programming Language :: Python :: 3.5',
              'Programming Language :: Python :: 3.6',
          ],
          tests_require=['mock', 'nose'],
          # set 
          # source folder? -> setup name?(package name)/output folder?
          # sourcedir -> CMakefiles.txt root folder.[default='']
          # output root Folder
          # ext_modules=[CMakeExtension('python-pcl', sourcedir='')],
          # output "python-pcl" Folder
          # ext_modules=[CMakeExtension('python-pcl/abc', sourcedir='')],
          # output "pcl" Folder
          # ext_modules=[CMakeExtension('pcl/python-pcl', sourcedir='')],
          # ext_modules=[CMakeExtension('pcl', sourcedir='')],
          ext_modules=[CMakeExtension('pcl/pcl')],
          cmdclass=dict(build_ext=CMakeBuild),
          data_files=data_files,
          test_suite='tests'
    )
