rem before install package
pip install -r requirements.txt
rem build source code
rem pip install -e .
python setup.py build_ext -i
python setup.py install
rem generate package
rem python setup.py bdist_wheel
rem api document update from sourceCode
rem sphinx-apidoc -F -o docs/source/reference src/
rem python setup.py bdist_wheel
nosetests -A "not pcl_ver_0_4"
